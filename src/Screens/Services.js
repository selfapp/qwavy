import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Dimensions,
    Image,
    SafeAreaView,
    FlatList
} from 'react-native';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import colors from '../styles/colors';
import General from "../styles/General";
import { BoldText, LightText } from '../Components/styledTexts';
import { Button } from '../Components/button';

const {height, width} = Dimensions.get('screen')

  
export default class Services extends Component{
    constructor(props){
        super(props);
    }
    
renderItem = ({ item}) =>{
    return(
        <View style={{ width: width/2, alignItems:'center', marginVertical:((width/2)-(width * 0.8/2))/2}}>
            <TouchableOpacity style={[ General.Card, {  height: height * 0.5 / 2, width: width * 0.8 / 2, alignItems:'center', justifyContent:'center'}]}
                            onPress = {()=>this.props.navigation.navigate('ServiceList',{serviceId: item.id, serviceName:item.title}) }
                        >
                            <Image
                                source = {{uri: item.image}}
                                style = {{ height: 80, width: 80 }}
                            />
                            <LightText style={{ marginTop:10, color: colors.gray, fontSize: 14 }}>{item.service_type}</LightText>
                            <LightText style={{ marginTop:5, color: colors.serviceTextColor, fontSize: 16 }}>{item.title}</LightText>
                        </TouchableOpacity>
        </View>
    )}

    render(){

        return(
            <View style={{flex: 1, backgroundColor:'#F5F5F5'}}>
                <SafeAreaView style={{ flex: 1}}>
                <View style={{flex:1, justifyContent:'space-evenly'}}>
                    <View style={{marginVertical: 30, justifyContent:'center', marginLeft: 30}}>
                        <BoldText style={{fontSize:20, color: colors.dimGray, fontWeight: '500'}}>Recommended Services</BoldText>
                    </View>
                    <View style={{ flex: 1 }}>
                    <Query query={GET_CATEGORIES}>
                            {({loading, error, data}) => {
                                if(loading){
                                    return <Loader loading={loading} />
                                }
                                if(error) {
                                    return <BoldText>{error}</BoldText>
                                }
                                console.log(data)
                                if(data) {
                                    if(data){
                                      return <FlatList
                                            data = {data.categories}
                                            numColumns={2}
                                            keyExtractor = {(item, index) => index.toString()}
                                            style={{ flex: 1 }}
                                            renderItem = {this.renderItem}
                                        />
                                    }else return <BoldText></BoldText>
                                } 
                            }}
                        </Query>
                    </View>
                    
                    {/* <View style={{flex: .4, marginHorizontal: 30 }}>
                    <View style={{flex: 1}}>
                            <Button
                                    style={{ borderRadius: 10, height: 50 }}
                                    value={'Continue'}
                                    color={colors.appColor}
                                    Light={true}
                                    textStyle={{fontSize: 20}}
                                    onPress ={() =>{
                                         this.props.navigation.navigate('ServiceList')
                                    }}
                                />
                            </View>
                    </View> */}
                    
                </View>
                </SafeAreaView>
            </View>
        )
    }
}

const GET_CATEGORIES = gql`
query {
    categories {
      id
      title
      image
      service_type
    }
  } `;