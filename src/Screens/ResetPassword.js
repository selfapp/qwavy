import React, { Component } from "react";
import {
    View,
    Image,
    Dimensions,
    ScrollView,
} from 'react-native';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import ErrorHandler from '../Components/ErrorHandler';
import colors from '../styles/colors';
import { Button } from '../Components/button';
import Loader from '../Components/Loader';
import { BoldText, LightText, TextInputField } from '../Components/styledTexts';


export default class ResetPassword extends Component {
    constructor() {
        super();
        this.state = {
            password: '',
            password_confirmation:' '
        }
    }

    onSuccess = (data) => {
        console.log('Success', data)
        if (data.resetPassword.success) {
           alert(data.resetPassword.message)
           this.props.navigation.goBack();
        } else {
            alert(data.resetPassword.message)
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Password & Security'}
                    />
                </View>
                <Mutation mutation={RESET_PASSWORD} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                    {(resetPassword, { loading }) => (

                <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                    <View style={{ flex: 1, marginTop: 30, marginHorizontal: 20, padding: 10, backgroundColor: '#fff', borderRadius: 5, shadowColor: '#451B2D', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.12, shadowRadius: 1, }}>
                        <BoldText style={{ fontSize: 18, color: colors.dimGray}}>Reset your password</BoldText>
                        <View style={{ flexDirection: 'row', marginTop: 20, alignItems:'center' }}>
                            <Image
                                source={require('../assets/new-password.png')}
                            />
                            <LightText style={{ marginLeft: 5}}>New password</LightText>
                        </View>

                            <View style={{ flex: 1, marginTop: 5 }}>
                                        <TextInputField
                                            style={{ flex: 1, paddingLeft: 20 }}
                                            placeholder={'Password'}
                                            secureTextEntry={true}
                                            onChangeText={(password) => this.setState({ password })}
                                            rightImage={true}
                                        />
                                        <View style={{ height: .5, backgroundColor: 'gray' }} />
                            </View>

                        <View style={{ flexDirection: 'row', marginTop: 20, alignItems:'center' }}>
                            <Image
                                source={require('../assets/new-password.png')}
                            />
                            <LightText style={{ marginLeft: 5}}>Confirm new password</LightText>
                        </View>

                            <View style={{ flex: 1, marginVertical: 5 }}>
                                        <TextInputField
                                            style={{ flex: 1, paddingLeft: 20 }}
                                            placeholder={'Password'}
                                            secureTextEntry={true}
                                            onChangeText={(password_confirmation) => this.setState({ password_confirmation  })}
                                            rightImage={true}
                                        />
                                        <View style={{ height: .5, marginBottom: 10, backgroundColor: 'gray' }} />
                            </View>
                    </View>
                    <View style={{ flex: 1, marginHorizontal: 25, marginTop: 30 }}>
                                    <Button
                                        style={{ borderRadius: 10, height: 40 }}
                                        value={'Save'}
                                        color={colors.appColor}
                                        Light={true}
                                        textStyle={{ fontSize: 20 }}
                                        onPress={() => {
                                            if (this.state.password.length > 1 && this.state.password_confirmation.length > 1 ) {
                                                resetPassword({ variables: this.state })
                                            } else {
                                                alert('All fields are required')
                                            }
                                        }}
                                    />
                                </View>
                                <Loader loading={loading} />
                </ScrollView>
                 )}
                 </Mutation>
            </View>
        )
    }
}

const RESET_PASSWORD = gql`
mutation resetPassword($password: String, $password_confirmation: String!){
    resetPassword(password: $password, password_confirmation: $password_confirmation) {
        success
        message
            }
          }`;