import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    FlatList,
    SafeAreaView,
    TouchableOpacity,
    Dimensions
} from "react-native";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import ErrorHandler from '../Components/ErrorHandler';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { LightText } from './../Components/styledTexts';
import Header from '../Components/Header';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from '../styles/colors';
import General from '../styles/General';

const { height, width } = Dimensions.get('screen');

export default class ShowPortfolio extends Component {
    constructor() {
        super();
        this.state = {
            refresh: false,
        }
    }
    componentDidMount(){
        this.props.navigation.addListener('willFocus', () =>{
            this.setState({ refresh: true });
         });
    }

    onSuccess = (data) => {
        console.log('Success', data)
        if (data.uploadBusinessProtfolio.success === "true") {
            AsyncStorage.setItem('profile', "true").then(() => {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'bottomTabNavigator' })],
            });
            this.props.navigation.dispatch(resetAction);
        })
        } else {
            alert(data.uploadBusinessProtfolio.message)
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Business Portfolio'}
                    />
                </View>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex:1, paddingHorizontal: 10, paddingVertical: 10 }}>
                        {/* Portfolio */}
                        <View style={{ flex:1, paddingVertical: 10,  backgroundColor: '#fff', shadowColor: '#451B2D', shadowOffset: { width: 0, height: 9 }, shadowOpacity: 0.12, shadowRadius: 21  }}>
                            <View style={{ paddingVertical: 15, marginHorizontal: 10, marginTop: 15, flexDirection:'row', justifyContent:'space-between' }}>
                                <LightText style={{ marginLeft: 10, color: colors.dimGray, fontSize: 14 }}>Business Portfolio</LightText>
                               <TouchableOpacity style={{ justifyContent:'space-between', alignItems:'center', backgroundColor: colors.appColor, padding: 5, paddingHorizontal:10, flexDirection:'row', borderRadius:20 }}
                                    onPress={()=> this.props.navigation.navigate('BusinessPortfolio', {fromProfile: true})}
                               >
                                    <Image
                                        source={require('../assets/plus.png')}
                                        style={{ height: 15, width: 15, tintColor: '#fff'}}
                                    />
                                    <LightText style={{ color: '#fff', marginLeft: 3}}>Add</LightText>
                                </TouchableOpacity>
                            </View>

                            <Query query={GET_PORTFOLIO}>
                            {({loading, error, data, refetch}) => {
                                if(loading){
                                    return <Loader loading={loading} />
                                }
                                if(error) {
                                    return <BoldText>{error}</BoldText>
                                }
                                console.log(data)
                                if(data) {
                                    if(data){
                           return <View style={{ flex:1 }}>
                           <FlatList
                                data={data.getBusinessPortfolio}
                                style={{ padding: 10 }}
                                numColumns={2}
                                extraData={(item, index) => index.toString()}
                                keyExtractor={(item, index) => index.toString()}
                                ItemSeparatorComponent={() => {
                                    return <View style={{ flex: 1, width: 10 }} />
                                }}
                                renderItem={({ item, index }) => (
                                    <View style={{ flex: 1 }}>
                                       
                                                <View style={{ flex: 1, paddingHorizontal: 10, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                                                    <View style={[General.Card, { flex: 1, marginVertical: 5, alignItems: 'center', justifyContent: 'center' }]}>
                                                        <Image
                                                             source={{ uri: `${item.image}` }}
                                                            style={{ borderRadius: 5, height: (width/2)-50, width: (width/2) - 50 }}
                                                            resizeMode={'contain'}
                                                        />
                                                    </View>
                                                    <TouchableOpacity style={{ position: 'absolute', height: 20, width: 20, alignItems: 'flex-start', justifyContent: 'flex-start' }}
                                                        // onPress={() => {
                                                        //     var array = [...this.state.portfolio];
                                                        //     array.splice(index, 1);
                                                        //     this.setState({ portfolio: array });
                                                        // }}
                                                    >
                                                        <Image
                                                            source={require('../assets/remove.png')}
                                                        />
                                                    </TouchableOpacity>
                                                </View>
                                           
                                    </View>
                                )}
                            />
                            {(this.state.refresh) ? (
                                this.setState({refresh: false}, ()=> refetch())
                                ): (null)}
                            </View>
                        }else return <BoldText></BoldText>
                    } 
                }}
            </Query>
                            
                        </View>
                    </View>
                    {/* <Loader loading={loading} /> */}
                </SafeAreaView>
            </View>
        )
    }
}


const GET_PORTFOLIO = gql`
query {
    getBusinessPortfolio {
      id
      image
    }
  } `;