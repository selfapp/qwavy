import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    SafeAreaView
} from "react-native";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import {
    LightText,
    BoldText,
    TextInputField
} from './../Components/styledTexts';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from '../styles/colors';
import ErrorHandler from '../Components/ErrorHandler';




export default class ForgotPassword extends Component {
    constructor() {
        super();
        this.state = {
            email:''
        };
    }

    onSuccess=(data)=>{
        console.log(JSON.stringify(data))
        alert(data.forgotPassword.message)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Mutation mutation={FORGOT_PASSWORD} onError={(error) => {ErrorHandler.showError(error)}} onCompleted={(data) =>{this.onSuccess(data)}}>
                    {(forgotPassword, { loading }) => (
                        <View style={{ flex: 1 }}>
                <View style={{ flex: 1, marginHorizontal: 30, alignItems: 'flex-start', justifyContent: 'flex-end' }}>
                    <Image
                        source={require('../assets/logo.png')}
                    />
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems:'center'}}>
                        <LightText style={{ fontWeight: '400', fontSize: 22, color: colors.dimGray}}>Forgot</LightText>
                        <BoldText style={{ fontSize: 24}}> Password?</BoldText>
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent:'space-evenly', paddingHorizontal: 30}}>
                <LightText>Lorem ipsum is simply dummy text of the printing and typesetting industry.</LightText>
                <View >
                        <LightText style={{ }}>Email ID</LightText>
                                <View style={{ marginTop: 5}}>
                                <TextInputField
                                         placeholder={'Email Address'}
                                         keyboardType={'email-address'}
                                         onChangeText={(email)=> this.setState({ email: email})}
                                    />
                                <View style={{height: 1, marginTop: 3, backgroundColor: 'gray'}}/>
                            </View>
                    </View>
                    
                </View>
                <View style={{ flex: 1, marginHorizontal: 30}}>
                    <View style={{flex: 1}}>
                            <Button
                                    style={{ borderRadius: 10, height: 50 }}
                                    value={'Log in'}
                                    color={colors.appColor}
                                    Light={true}
                                    textStyle={{fontSize: 20}}
                                    onPress ={() =>{
                                        if(this.state.email.length > 1){
                                        forgotPassword({ variables: { email: this.state.email}})
                                        }else{
                                            alert(`Email can't blank`)
                                        }
                                    }}
                                />
                            </View>
                            <View style={{ alignItems:'center'}}>
                            <Text style={{fontSize: 16, marginTop:30}}>Already have account?
                                <Text style={{fontWeight: '800', color: colors.appColor}} onPress={() => this.props.navigation.goBack()}> Log in</Text>
                            </Text>
                            </View>
                            <View style={{ height: 30}}/>
                </View>
                <Loader loading={loading} />
                </View>
                )}     
                </Mutation>

            </SafeAreaView>
        )
    }
}

const FORGOT_PASSWORD = gql`
mutation forgotPassword($email: String!){
    forgotPassword(email: $email) {
                success,
                message
            }
          }`;