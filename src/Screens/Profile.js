import React, { Component } from "react";
import {
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    SafeAreaView
} from 'react-native';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import colors from '../styles/colors';
import { BoldText, LightText } from '../Components/styledTexts';



const { height, width } = Dimensions.get('screen')

export default class Profile extends Component {
    constructor() {
        super();
        this.state={
            refresh: false,
        }
    }
    componentDidMount(){
        this.props.navigation.addListener('willFocus', () =>{
            this.setState({ refresh: true });
         });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'My Profile'}
                    />
                </View>
                <Query query={GET_USER} pollInterval={500}>
                    {({ loading, error, data, refetch }) => {
                        if (loading) {
                            return <Loader loading={loading} />
                        }
                        if (error) {
                            console.log(error)
                            return <BoldText></BoldText>
                        }
                        if (data) {
                            console.log(data)

                            return <SafeAreaView style={{ flex: 1 }}>
                                <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>

                                    <View style={{ flex: 1, justifyContent: 'center', paddingVertical: 20, flexDirection: 'row' }}>
                                        <View style={{ flex: 2, marginHorizontal: 15, justifyContent: 'center' }}>
                                            <BoldText style={{ color: colors.appColor, marginTop: 5 }}>{data.me.name}</BoldText>
                                            <View style={{ flexDirection: 'row', alignItems:'center', marginTop: 5}}>
                                                <Image
                                                    source={ require('../assets/location-pin.png')}
                                                />
                                                <LightText style={{ color: colors.gray, marginLeft: 5 }}>{`${data.me.city}, ${data.me.state}`}</LightText>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                                                <View style={{ height: width / 4, width: width / 4, borderRadius: (width / 4) / 2, borderWidth: 2, borderColor: colors.lightGray, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Image
                                                        style={{ height: width / 4 - 10, width: width / 4 - 10, borderRadius: (width / 4 - 10) / 2, }}
                                                        source={(data.me.picture) ? ({ uri: data.me.picture }) : require("../assets/logo.png")}
                                                        resizeMode={'contain'}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, marginHorizontal: 20, paddingVertical: 10, flexDirection: 'row', backgroundColor: '#fff', borderRadius: 10, shadowColor: '#451B2D', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.12, shadowRadius: 1, }}>
                                        <View style={{ flex: 1, alignItems: 'center' }}>
                                            <BoldText style={{ color: colors.appColor, marginTop: 5 }}>{data.me.joining_date}</BoldText>
                                            <LightText style={{ color: colors.gray, marginTop: 5 }}>{`Joining Date`}</LightText>
                                        </View>
                                        <View style={{ marginVertical: 5, width: .5, backgroundColor: colors.dimGray }} />
                                        <View style={{ flex: 1, alignItems: 'center' }}>
                                            <BoldText style={{ color: colors.appColor, marginTop: 5 }}>{`25`}</BoldText>
                                            <LightText style={{ color: colors.gray, marginTop: 5 }}>{`Successfull Projects`}</LightText>
                                        </View>
                                    </View>

                                    <View style={{ flex: 1, marginHorizontal: 20, marginVertical: 50, }}>
                                        <TouchableOpacity style={{ flex: 1, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}
                                            onPress={()=> this.props.navigation.navigate('EditProfile')}
                                        >
                                            <Image
                                                source={require('../assets/profile.png')}
                                                style={{ tintColor: colors.appColor }}
                                            />
                                                <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>Profile</LightText>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 1, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                                            <Image
                                                source={require('../assets/wallet.png')}
                                                style={{ tintColor: colors.appColor }}
                                            />
                                                <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>Payment</LightText>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 1, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                                            <Image
                                                source={require('../assets/identification.png')}
                                                style={{ tintColor: colors.appColor }}
                                            />
                                                <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>License</LightText>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 1, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}
                                            onPress={()=> this.props.navigation.navigate('ShowPortfolio')}
                                        >
                                            <Image
                                                source={require('../assets/briefcase.png')}
                                                style={{ tintColor: colors.appColor }}
                                            />
                                                <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>Business Portfolio</LightText>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ flex: 1, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}
                                            onPress={()=> this.props.navigation.navigate('Setting')}
                                        >
                                            <Image
                                                source={require('../assets/settings.png')}
                                                style={{ tintColor: colors.appColor }}
                                            />
                                                <LightText style={{marginLeft: 10, color: colors.gray, fontSize: 16 }}>Settings</LightText>
                                        </TouchableOpacity>
                                        {(this.state.refresh) ? (
                                this.setState({refresh: false}, ()=> refetch())
                                ): (null)}
                                    </View>
                                </ScrollView>
                            </SafeAreaView>
                        }
                        return <BoldText />
                    }
                    }
                </Query>
            </View>
        )
    }
}

const GET_USER = gql`
query {
    me {
    name
    email
    mobile
    business_name
    city
    state
    zip
    picture
    tax_id
    street_address
    success
    message
    joining_date
    }
} `;