import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    FlatList,
    SafeAreaView,
    TouchableOpacity
} from "react-native";
import {
    LightText,
} from './../Components/styledTexts';
import Header from '../Components/Header';
import Loader from '../Components/Loader';


const DATA = [{ name: 'abc'},{ name: 'abc'},{ name: 'abc'},{ name: 'abc'},{ name: 'abc'},]


export default class Jobs extends Component {
    constructor() {
        super();
        this.state = {
        };
    }

    renderItem = ({ item}) =>{
        return(
            <View style={{paddingVertical: 10,}}>
            <View style={{ flex: 1, borderRadius: 1, backgroundColor: '#FFF', paddingVertical: 10,  shadowOffset:{width:0, height:2}, shadowOpacity: 0.18, shadowRadius:1, shadowColor: '#451B2D', elevation: 3 }}>
                <View style={{ flex: 1, flexDirection:'row',}}>
                    <Image
                        source={require('../assets/Oval_.png')}
                    />
                    <View style={{ flex: 1, marginLeft: 10, justifyContent:'center'}}>
                        <LightText style={{ fontSize: 18}}>{item.name}</LightText>
                        <View style={{ flex: 1, flexDirection:'row', marginTop: 5}}>
                        <View style={{ flex: 1}}>
                            <LightText>Location:<LightText style={{ fontSize: 12}}> New york, USA </LightText></LightText>
                        </View>
                        <View style={{ flex: 1}}>
                        <LightText>Budget:<LightText style={{ fontSize: 12}}>$1000-$2000</LightText></LightText>
                        </View>
                        </View>
                    </View>
                </View>
            </View>
            </View>
        )}
        separator = () => <View style={{height: 10}} />;


    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{flex: .12}}>
                    <Header
                    // leftNavigation={ this.props.navigation}
                    value={'QWavy'}
                    />
                </View>
            <SafeAreaView style={{ flex: 1 }}>
                
                <View style={{ flex: 1}}>
                <FlatList
                    data = {DATA}
                    keyExtractor = {(item, index) => index.toString()}
                    style={{ flex: 1 }}
                    renderItem = {this.renderItem}
                    ItemSeparatorComponent={this.separator}
                />

                </View>  
                <Loader loading={this.props.loading} />
            </SafeAreaView>
            </View>
        )
    }
}
