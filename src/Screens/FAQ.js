import React, { Component } from "react";
import {
    View,
    Image
} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';

import colors from '../styles/colors';
import { BoldText, LightText } from '../Components/styledTexts';

const SECTIONS = [
    {
      title: 'What is website FAQ?',
      content: 'Designing Effective FAQ Pages. FAQ stands for “frequently asked questions.” As the name implies, it is a type of web page (or group of web pages) that lists questions frequently asked by users, usually about different aspects of the website or its services. ... FAQ pages aim to make finding answers easy for users.',
    },
    {
      title: 'How to stop notifications to my email?',
      content: 'Lorem ipsum...',
    },
    {
        title: 'What is the purpose of an FAQ?',
        content: 'The purpose of an FAQ is generally to provide information on frequent questions or concerns; however, the format is a useful means of organizing information, and text consisting of questions and their answers may thus be called an FAQ regardless of whether the questions are actually frequently asked.',
      },
      {
        title: `Should it be FAQ's or FAQs?`,
        content: 'I disagree that FAQ always stands for Frequently Asked Questions; a single question that is frequently asked is also a frequently asked question, or FAQ. Thus, a list of such questions is a list of FAQs. A FAQ is a collection of Frequently Asked Question(s) for a specific topic.',
      },
      {
        title: 'What is a FAQ document?',
        content: 'Pronounced as separate letters, or as "fak," and short for frequently asked questions, a FAQ is an online document that poses a series of common questions and answers on a specific topic. FAQs originated in Usenet groups as a way to answer questions about the rules of the service.        ',
      },
      {
        title: 'What is the full meaning of FAQs?',
        content: 'The Full form of FAQ is Frequently Asked Questions. FAQs are listed questions and answers pertaining to a particular topic, all supposed to be commonly asked in some context. These are commonly used where certain common questions tend to recur on email mailing lists and other online forums.',
      },
      {
        title: 'What is FAQ in Mobile?',
        content: 'Mobile App FAQ. Klipfolio lets you track and share data that matters to you.',
      },
  ];

export default class FAQ extends Component {
    constructor() {
        super();
        this.state = {
        activeSections: [],
      };
    }
    
    
      _renderHeader = (section, index, isActive) => {
        return (
            <View style={{paddingVertical: 5}}>
            <View style={{ paddingHorizontal: 5, paddingVertical: 5, flexDirection:'row', alignItems:'center' }}>
              <View style={{backgroundColor: colors.appColor, padding: 5, height: 30, width: 30, borderRadius: 15, alignItems:'center', justifyContent: 'center' }}>
                    <LightText style={{ color: '#fff'}}>{index+1}</LightText>
              </View>
              <View style={{ marginHorizontal: 10, flex: 1}}>
                    <LightText style={{ fontSize: 16 }}>{`${section.title}`}</LightText>
            </View>
            <View style={{ alignItems:'center', justifyContent:'center'}}>
              <Image
                source = {require('../assets/forwordArrow.png')}
                style = {{ transform: [{ rotate: isActive ? '90deg' : '0deg'}]}}
              />
            </View>
          </View>
          { !isActive &&
            <View style={{ marginHorizontal: 10, marginLeft: 50, height: .5, backgroundColor:colors.gray}}/>
          }
          </View>
        );
      };
    
      _renderContent = (section) => {
        return (
            <View>
            <View style={{ marginHorizontal: 10, marginLeft: 50, paddingVertical: 5, flexDirection:'row', alignItems:'center' }}>
                 <LightText style={{ color: colors.gray}}>{section.content}</LightText>
          </View>
            <View style={{ marginHorizontal: 10, marginLeft: 50, height: .5, backgroundColor:colors.gray}}/>
          </View>
        );
      };
    
      _updateSections = activeSections => {
        this.setState({ activeSections });
      };

    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={`FAQ`}
                    />
                </View>
                <View style={{ flex: 1, backgroundColor: '#fff', padding: 10, paddingHorizontal: 15 }}>
                <View style={{ marginBottom: 20}}>
                    <BoldText style={{ color: colors.gray}}>FAQ</BoldText>
                </View>
                <Accordion
                    sections={SECTIONS}
                    underlayColor={colors.lightGray}
                    activeSections={this.state.activeSections}
                    renderHeader={this._renderHeader}
                    renderContent={this._renderContent}
                    onChange={this._updateSections}
                />
                </View>
            </View>
        )
    }
}
