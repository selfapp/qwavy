import React, { Component } from "react";
import {
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    SafeAreaView
} from 'react-native';
import colors from '../styles/colors';
import { BoldText, LightText } from '../Components/styledTexts';



const { height, width } = Dimensions.get('screen')

export default class About extends Component {
    constructor() {
        super();
    }


    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'About Us'}
                    />
                </View>
                <ScrollView style={{ flex: 1, backgroundColor: '#fff', padding: 10, paddingHorizontal: 15 }}>
                    <BoldText style={{ fontSize: 18, color: colors.dimGray}}>Lorem ipsum</BoldText>
                    
                    <LightText style={{ fontSize: 14, marginVertical:15, color: colors.gray, textAlign:'justify'}}>
                     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </LightText>
                </ScrollView>
            </View>
        )
    }
}
