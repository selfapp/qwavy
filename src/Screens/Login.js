import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    SafeAreaView,
    TouchableOpacity
} from "react-native";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import ErrorHandler from '../Components/ErrorHandler';
import {
    LightText,
    BoldText,
    TextInputField
} from './../Components/styledTexts';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from '../styles/colors';



export default class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: ""
        }
    }

    onSuccess = (data) => {
        console.log('Success', data)
        if (data.login.success === 'true') {
            AsyncStorage.setItem('token', data.login.access_token).then(() => {
                AsyncStorage.setItem('isProfile', data.login.is_profile_completed)
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: (data.login.is_profile_completed === 'true') ? 'bottomTabNavigator' : 'completeProfile' })],
                });
                this.props.navigation.dispatch(resetAction);
            })
        } else {
            alert(data.login.message)
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Mutation mutation={LOGIN} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                    {(login, { loading }) => (
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1, marginHorizontal: 30, alignItems: 'flex-start', justifyContent: 'flex-end' }}>
                                <Image
                                    source={require('../assets/logo.png')}
                                />
                                <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                    <LightText style={{ fontWeight: '400', fontSize: 20, color: colors.dimGray }}>Log</LightText>
                                    <BoldText style={{ fontSize: 24 }}> in</BoldText>
                                </View>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'space-evenly', paddingHorizontal: 30 }}>
                                <View >
                                    <LightText style={{}}>Email ID</LightText>
                                    <View style={{ marginTop: 5 }}>
                                        <TextInputField
                                            placeholder={'Email Address'}
                                            keyboardType={'email-address'}
                                            onChangeText={(email) => this.setState({ email: email })}
                                        />
                                        <View style={{ height: .5, backgroundColor: 'gray' }} />
                                    </View>
                                </View>
                                <View>
                                    <LightText style={{}}>Password</LightText>
                                    <View style={{ marginTop: 5 }}>
                                        <TextInputField
                                            placeholder={'Password'}
                                            secureTextEntry={true}
                                            onChangeText={(password) => this.setState({ password: password })}
                                        />
                                        <View style={{ height: .5, backgroundColor: 'gray' }} />
                                    </View>
                                </View>
                                <View style={{ alignItems: 'flex-end' }}>
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate('ForgotPassword')}
                                    >
                                        <LightText style={{ color: '#000' }}> Forgot password?</LightText>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ flex: 1, marginHorizontal: 30, marginTop: 15 }}>
                                <View style={{ flex: 1 }}>
                                    <Button
                                        style={{ borderRadius: 10, height: 50 }}
                                        value={'Log in'}
                                        color={colors.appColor}
                                        Light={true}
                                        textStyle={{ fontSize: 20 }}
                                        onPress={() => {
                                            if (this.state.email.length > 1 & this.state.password.length > 1) {
                                                login({ variables: { email: `${this.state.email}`, password: `${this.state.password}` } })
                                            } else {
                                                alert('Please enter valid Login detail')
                                            }
                                        }}
                                    />
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ fontSize: 16, marginTop: 30 }}>Don't have an account?
                                <Text style={{ fontWeight: '800', color: colors.appColor }} onPress={() => this.props.navigation.navigate('SignUp')}> Signup</Text>
                                    </Text>
                                </View>
                                <View style={{ height: 30 }} />
                            </View>
                            <Loader loading={loading} />
                        </View>
                    )}
                </Mutation>
            </SafeAreaView>
        )
    }
}

const LOGIN = gql`
mutation login($email: String!, $password: String!){
    login(email: $email, password: $password) {
        id
        name
        access_token
        success
        message
        is_profile_completed
            }
          }`;