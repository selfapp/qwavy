import React, { Component } from "react";
import {
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import colors from '../styles/colors';
import {Button} from '../Components/button';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import { BoldText, LightText } from '../Components/styledTexts';

const { height, width } = Dimensions.get('screen')

export default class Setting extends Component {
    constructor() {
        super();
    }


    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: .15 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Settings'}
                    />
                </View>
                <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                    <View style={{ flex: 1, marginHorizontal: 20, marginVertical: 10, }}>
                        <TouchableOpacity style={{ flex: 1, marginVertical: 15, flexDirection: 'row', alignItems: 'center' }}
                            onPress={()=> this.props.navigation.navigate('ResetPassword')}
                        >
                            <Image
                                source={require('../assets/password.png')}
                                style={{ tintColor: colors.appColor }}
                            />
                            <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>{`Password & Security`}</LightText>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, marginVertical: 15, flexDirection: 'row', alignItems: 'center' }}
                            onPress={()=> this.props.navigation.navigate('About')}
                        >
                            <Image
                                source={require('../assets/about.png')}
                                style={{ tintColor: colors.appColor }}
                            />
                            <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>About Us</LightText>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, marginVertical: 15, flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={require('../assets/terms.png')}
                                style={{ tintColor: colors.appColor }}
                            />
                            <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>Terms and conditions</LightText>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, marginVertical: 15, flexDirection: 'row', alignItems: 'center' }}
                            onPress={()=> this.props.navigation.navigate('FAQ')}
                        >
                            <Image
                                source={require('../assets/faq.png')}
                                style={{ tintColor: colors.appColor }}
                            />
                            <LightText style={{ marginLeft: 10, color: colors.gray, fontSize: 16 }}>FAQ</LightText>
                        </TouchableOpacity>
                        
                    </View>
                    
                </ScrollView>
                 <View style={{ marginVertical: 10, marginHorizontal: 20 }}>
                        <Button
                            style={{ borderRadius: 10, height: 45 }}
                            value={'Logout'}
                            color={colors.appColor}
                            Light={true}
                            textStyle={{ fontSize: 20 }}
                            onPress={() => {
                                AsyncStorage.clear();
                                                const resetAction = StackActions.reset({
                                                    index: 0,
                                                    actions: [NavigationActions.navigate({ routeName: 'Splash' })],
                                                  });
                                                  this.props.navigation.dispatch(resetAction);
                            }}
                        />
                    </View>
            </View>
        )
    }
}
