import React, { Component } from "react";
import {
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    SafeAreaView,
    KeyboardAvoidingView
} from 'react-native';
import gql from "graphql-tag";
import { Mutation, Query } from "react-apollo";
import ErrorHandler from '../Components/ErrorHandler';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import colors from '../styles/colors';
import { BoldText, LightText, TextInputField } from '../Components/styledTexts';
import { Button } from '../Components/button';



const { height, width } = Dimensions.get('screen')

export default class PersonalInformation extends Component {
    constructor() {
        super();
        this.state = {
            picture:null,
            name: '',
            mobile: '',
            email: '',
            business_name: '',
            tax_id:'',
            street_address:'',
            city: '',
            state: '',
            zip: ''
        };
    }

    onSuccess = (data) => {
        console.log('Success', data)
        if (data.personalInformation.success === "true") {
            this.props.navigation.navigate('UploadDocuments')
        } else {
            alert(data.personalInformation.message)
        }
    }
    getUserData(user){
        console.log('Success', user)
        if(user.me.success === "true"){
            this.setState({ name: user.me.name, mobile: user.me.mobile, email: user.me.email, business_name: user.me.business_name, tax_id: user.me.tax_id, street_address: user.me.street_address, city: user.me.city, state: user.me.state, zip: user.me.zip, picture: user.me.picture})
        }else{
            alert(user.me.message)
        }
    }


    openGalleryForProfilePic(buttonIndex) {

        if(buttonIndex === 1) {
            ImagePicker.openPicker({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    this.setState({
                        picture: `data:${image.mime};base64,${image.data}`,
                    });
                });
        } else if(buttonIndex === 0) {
            ImagePicker.openCamera({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    this.setState({
                        picture: `data:${image.mime};base64,${image.data}`,
                    });
                });
        }
    }

    showActionSheet = () => {
    this.ActionSheet.show()
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Personal Information'}
                    />
                </View>
                <Mutation mutation={PERSONAL_INFORMATION} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                    {(personalInformation, { loading }) => (
                <SafeAreaView style={{ flex: 1 }}>
                    
                    <ScrollView style={{ flex: 1}}>
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <KeyboardAvoidingView
                                        contentContainerStyle={{ flex: 1 }}
                                        behavior='position'
                                        keyboardVerticalOffset={-500}
                                        enabled
                                    >
                        <View style={{ flex: 1 }}>
                    
                        <View style={{ flex: 1, backgroundColor: '#fff' }}>
                        
                            <View style={{ flex: 1, justifyContent: 'center', paddingVertical: 20 }}>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ height: width / 4, width: width / 4, borderRadius: (width / 4) / 2, borderWidth: 2, borderColor: colors.lightGray, justifyContent: 'center', alignItems: 'center' }}
                                         onPress={()=> this.showActionSheet()}
                                        >
                                            <Image
                                                style={{ height: width / 4 - 10, width: width / 4 - 10, borderRadius: (width / 4 - 10) / 2, }}
                                                source={(this.state.picture) ? ({ uri: this.state.picture }) : require("../assets/logo.png")}
                                                resizeMode={'contain'}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <LightText style={{ color: colors.gray, marginTop: 5 }}>{`Upload photo`}</LightText>

                                    <ActionSheet
                                                ref={o => this.ActionSheet = o}
                                                title={'Which one do you like ?'}
                                                options={['Take photo', 'Choose from Library', 'Cancel']}
                                                cancelButtonIndex={2}
                                                onPress={(index) => { this.openGalleryForProfilePic(index) }}
                                                />
                                </View>
                            </View>
                            <View style={{ flex: 1, backgroundColor: colors.white }}>
                            <Query query={GET_USER} onCompleted={(data)=>{this.getUserData(data)}}>
                            {({loading, error}) => {
                                if(loading){
                                    return <Loader loading={loading} />
                                }
                                if(error) {
                                    return <BoldText>{error}</BoldText>
                                }
                                return <BoldText/>
                            }
                            }
                                </Query>

                                <View style={{ marginHorizontal: 20, marginTop: 20 }}>
                                    <View style={{ flex: 1.5, marginHorizontal: 15, justifyContent: 'space-around' }}>

                                        <View style={{ flex: 1, marginVertical: 5 }}>
                                            <LightText style={{ marginLeft: 10 }}>Full name</LightText>
                                            <View style={{ marginHorizontal: 10, }}>
                                                <TextInputField
                                                    placeholder={'Enter Full Name'}
                                                    value={this.state.name}
                                                    onChangeText={(name)=> this.setState({name})}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>
                                        <View style={{ flex: 1, marginVertical: 10 }}>
                                            <LightText style={{ marginLeft: 10 }}>Mobile Number</LightText>
                                            <View style={{ marginHorizontal: 10 }}>

                                                <TextInputField
                                                    placeholder={'Enter Mobile Number'}
                                                    value={this.state.mobile}
                                                    maxLength={10}
                                                    keyboardType={'number-pad'}
                                                    onChangeText={(mobile)=> this.setState({ mobile })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>
                                        <View style={{ flex: 1, marginVertical: 5 }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <LightText style={{ marginLeft: 10 }}>Email Address</LightText>
                                            </View>
                                            <View style={{ marginHorizontal: 10 }}>
                                                <TextInputField
                                                    value={this.state.email}
                                                    editable={false}
                                                    // onChangeText={(email)=> this.setState({ email })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>

                                        <View style={{ marginVertical: 20 }}>
                                            <BoldText style={{ color: colors.dimGray}}> Business Information</BoldText>
                                        </View>

                                        <View style={{ flex: 1, marginVertical: 5 }}>

                                            <LightText style={{ marginLeft: 10 }}>Business Name</LightText>
                                            <View style={{ marginHorizontal: 10 }}>
                                                <TextInputField
                                                    placeholder={'Business name'}
                                                    value={this.state.business_name}
                                                    onChangeText={(business_name)=> this.setState({ business_name })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>

                                        <View style={{ flex: 1, marginVertical: 5 }}>

                                            <LightText style={{ marginLeft: 10 }}>Tax ID</LightText>
                                            <View style={{ marginHorizontal: 10 }}>
                                                <TextInputField
                                                    placeholder={'Tax ID'}
                                                    value={this.state.tax_id}
                                                    onChangeText={(tax_id)=> this.setState({ tax_id })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>

                                        <View style={{ flex: 1, marginVertical: 5 }}>

                                            <LightText style={{ marginLeft: 10 }}>Street Address</LightText>
                                            <View style={{ marginHorizontal: 10 }}>
                                                <TextInputField
                                                    placeholder={'Street Address'}
                                                    value={this.state.street_address}
                                                    onChangeText={(street_address)=> this.setState({ street_address })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>

                                        <View style={{ flex: 1, marginVertical: 5 }}>
                                            <LightText style={{ marginLeft: 10 }}>City</LightText>
                                            <View style={{ marginHorizontal: 10 }}>
                                                <TextInputField
                                                    placeholder={'City'}
                                                    value={this.state.city}
                                                    onChangeText={(city)=> this.setState({ city })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>

                                        <View style={{ flex: 1, marginVertical: 5 }}>
                                            <LightText style={{ marginLeft: 10 }}>State</LightText>
                                            <View style={{ marginHorizontal: 10 }}>
                                                <TextInputField
                                                    placeholder={'State'}
                                                    value={this.state.state}
                                                    onChangeText={(state)=> this.setState({ state })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>

                                        <View style={{ flex: 1, marginVertical: 5 }}>

                                            <LightText style={{ marginLeft: 10 }}>Zip code</LightText>
                                            <View style={{ marginHorizontal: 10 }}>
                                                <TextInputField
                                                    placeholder={'Zip Code'}
                                                    value={this.state.zip}
                                                    maxLength={6}
                                                    keyboardType={'number-pad'}
                                                    onChangeText={(zip)=> this.setState({ zip })}
                                                />
                                            </View>
                                            <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                        </View>

                                    </View>
                                </View>
                                <View style={{ flex: 1, marginHorizontal: 30, marginVertical: 50 }}>
                                    <Button
                                        style={{ borderRadius: 10, height: 50 }}
                                        value={'Next'}
                                        color={colors.appColor}
                                        Light={true}
                                        textStyle={{ fontSize: 20 }}
                                        onPress={() => {
                                            var { name, mobile, email, business_name, tax_id, street_address, city, state, zip} = this.state; 
                                            console.log(JSON.stringify(this.state))    
                                            if (name !== null & mobile !== null && email !== null && business_name !== null && tax_id !== null, street_address !== null && city !== null && state !== null && zip !== null) {
                                                console.log(JSON.stringify({ variables: {input: this.state} }))
                                                personalInformation({ variables: {input: this.state} })
                                            } else {
                                                alert('All fields are required')
                                            }
                                        }}
                                    />
                                </View>
                            </View>
                            
                        </View>
                         
                         </View>
                         </KeyboardAvoidingView>
                    </TouchableWithoutFeedback>
                    </ScrollView>
                    <Loader loading={loading} />
                </SafeAreaView>
                )}
                </Mutation>
            </View>
        )
    }
}

const PERSONAL_INFORMATION = gql`
mutation personalInformation($input: UpdateProfileInput!){
    personalInformation(input: $input) {
        success
        message
            }
          }`;

const GET_USER = gql`
query {
    me {
    id
    name
    email
    mobile
    business_name
    city
    state
    zip
    picture
    tax_id
    street_address
    success
    message
    }
} `;