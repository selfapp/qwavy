import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    FlatList,
    SafeAreaView,
    TouchableOpacity,
    Dimensions
} from "react-native";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import ErrorHandler from '../Components/ErrorHandler';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import { LightText } from './../Components/styledTexts';
import Header from '../Components/Header';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from '../styles/colors';
import General from '../styles/General';

const { height, width } = Dimensions.get('screen');

export default class BusinessPortfolio extends Component {
    constructor() {
        super();
        this.state = {
            portfolio: ['add'],
            fromProfile: false
        };
    }
    componentDidMount(){
        this.setState( { fromProfile: this.props.navigation.state.params.fromProfile})
    }

    onSuccess = (data) => {
        console.log('Success', data)
        if (data.uploadBusinessProtfolio.success === "true") {
            if(!this.state.fromProfile){
            AsyncStorage.setItem('profile', "true").then(() => {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'bottomTabNavigator' })],
            });
            this.props.navigation.dispatch(resetAction);
        })
    }else{
        this.props.navigation.goBack();
    }
        } else {
            alert(data.uploadBusinessProtfolio.message)
        }
    }

    openGalleryForProfilePic(buttonIndex) {

        if (buttonIndex === 1) {
            ImagePicker.openPicker({
                width: 1000,
                height: 1000,
                compressImageQuality:0.4,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {

                    let obj = `data:${image.mime};base64,${image.data}` 
                    let images = this.state.portfolio;
                    images = [...images, obj]
                    this.setState({ portfolio: images })

                });
        } else if (buttonIndex === 0) {
            ImagePicker.openCamera({
                width: 1000,
                height: 1000,
                compressImageQuality: 0.4,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    let obj = `data:${image.mime};base64,${image.data}`
                    let images = this.state.portfolio;
                    images = [...images, obj]
                    this.setState({ portfolio: images })
                });
        }
    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Business Portfolio'}
                    />
                </View>
                <Mutation mutation={UPLOAD_PORTFOLIO} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                {(uploadPortfolio, { loading }) => (
                <SafeAreaView style={{ flex: 1 }}>

                    <View style={{ flex: 1 }}>
                        {/* Portfolio */}
                        <View style={{ flex: 1 }}>
                            <View style={{ marginHorizontal: 10, marginTop: 15 }}>
                                <TouchableOpacity style={{ height: 40, borderRadius: 5, backgroundColor: 'rgb(200,240,227)', flexDirection: 'row', alignItems: 'center', marginVertical: 20, borderStyle: 'dashed', borderWidth: .5, borderColor: 'gray' }}
                                    onPress={() => {
                                        this.setState({ isLicence: true })
                                        this.showActionSheet()
                                    }}
                                >
                                    <Image
                                        style={{ height: 25, width: 25, marginHorizontal: 20 }}
                                        source={require("../assets/photo-camera.png")}
                                        resizeMode={'contain'}
                                    />
                                    <LightText style={{ marginLeft: 10, color: colors.dimGray, fontSize: 14 }}>Upload Pictures</LightText>
                                </TouchableOpacity>
                            </View>
                            <FlatList
                                data={this.state.portfolio}
                                style={{ padding: 10 }}
                                extraData={(item, index) => index.toString()}
                                keyExtractor={(item, index) => index.toString()}
                                ItemSeparatorComponent={() => {
                                    return <View style={{ flex: 1, width: 5 }} />
                                }}
                                renderItem={({ item, index }) => (
                                    <View style={{ flex: 1 }}>
                                        {item === 'add' ? (
                                            <TouchableOpacity
                                                style={[General.Card, { flex: 1, paddingVertical: 20, alignItems: 'center' }]}
                                                onPress={() => this.ActionSheet.show()}
                                            >
                                                <Image
                                                    source={require('../assets/add-icon.png')}
                                                    style={{ height: 35, width: 35 }}
                                                    resizeMode={'contain'}
                                                />
                                                <LightText style={{ marginTop: 10, color: colors.dimGray, fontWeight: '500', fontSize: 12, textAlign: 'center' }}>Upload Image</LightText>
                                            </TouchableOpacity>
                                        ) : (
                                                <View style={{ flex: 1, paddingHorizontal: 5, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                                                    <View style={[General.Card, { flex: 1, marginVertical: 5, alignItems: 'center', justifyContent: 'center', }]}>
                                                        <Image
                                                            source={{ uri: `${item}` }}
                                                            style={{ borderRadius: 5, height: width / 2, width: width - 50 }}
                                                            resizeMode={'cover'}
                                                        />
                                                    </View>
                                                    <TouchableOpacity style={{ position: 'absolute', height: 20, width: 20, alignItems: 'flex-start', justifyContent: 'flex-start' }}
                                                        onPress={() => {
                                                            var array = [...this.state.portfolio];
                                                            array.splice(index, 1);
                                                            this.setState({ portfolio: array });
                                                        }}
                                                    >
                                                        <Image
                                                            source={require('../assets/remove.png')}
                                                        />
                                                    </TouchableOpacity>
                                                </View>
                                            )}
                                    </View>
                                )}
                            />
                        </View>
                        {/* Action sheet */}
                        <ActionSheet
                            ref={o => this.ActionSheet = o}
                            title={'Which one do you like ?'}
                            options={['Take photo', 'Choose from Library', 'Cancel']}
                            cancelButtonIndex={2}
                            onPress={(index) => { this.openGalleryForProfilePic(index) }}
                        />

                    </View>
                    <View style={{ marginVertical: 20, marginHorizontal: 10 }}>
                        <Button
                            style={{ borderRadius: 10, height: 45 }}
                            value={'Next'}
                            color={colors.appColor}
                            Light={true}
                            textStyle={{ fontSize: 20 }}
                            onPress={() => {
                                console.log(JSON.stringify({ variables: { portfolio: this.state.portfolio }}))
                                 uploadPortfolio({ variables: { portfolio: this.state.portfolio }})
                                // this.props.navigation.navigate('bottomTabNavigator')
                            }}
                        />
                    </View>
                    <Loader loading={loading} />
                </SafeAreaView>
                 )}
                 </Mutation>
            </View>
        )
    }
}

const UPLOAD_PORTFOLIO = gql`
mutation uploadBusinessProtfolio($portfolio: [String]){
    uploadBusinessProtfolio(portfolio: $portfolio) {
        success
        message
            }
          }`;
