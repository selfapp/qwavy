import React, {Component} from 'react';
import {
    View,
    Image
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

export default class Splash extends Component{
    constructor(props){
        super(props);
    }
 
 async componentDidMount(){
        const userToken = await AsyncStorage.getItem('token')
        if(userToken !== null){
            AsyncStorage.getItem('isProfile').then((data) => {
                if(data === 'true'){
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'bottomTabNavigator' })],
                    });
                    this.props.navigation.dispatch(resetAction);
                }else{
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'completeProfile' })],
                      });
                      this.props.navigation.dispatch(resetAction);
                }
            })
            
        }else{
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'signUpNaviationOptions' })],
              });
              
              this.props.navigation.dispatch(resetAction);
        }
    }

    render(){
        return(
                <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                 <View style={{ flex:1, alignItems:'center', justifyContent:'center'}}>
                    <Image
                        source={require('../assets/logo.png')}
                    />
                </View>
                <View style={{ flex: 1}}>
                <Image
                        source={require('../assets/img.png')}
                    />
                </View>
               </View>
        )
    }
}
