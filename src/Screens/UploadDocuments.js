import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    FlatList,
    SafeAreaView,
    TouchableOpacity
} from "react-native";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import ErrorHandler from '../Components/ErrorHandler';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import {
    LightText
} from './../Components/styledTexts';
import Header from '../Components/Header';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from '../styles/colors';
import General from '../styles/General';


export default class UploadDocuments extends Component {
    constructor() {
        super();
        this.state = {
            driving_licence: [],
            certification_id: [],
            isLicence: false,
            flatListheight: 0
        };
    }

    onSuccess = (data) => {
        console.log('Success', data)
        if (data.uploadYourLicence.success === "true") {
            this.props.navigation.navigate('BusinessPortfolio', {fromProfile: false})
        } else {
            alert(data.uploadYourLicence.message)
        }
    }

    openGalleryForProfilePic(buttonIndex) {

        if (buttonIndex === 1) {
            ImagePicker.openPicker({
                width: 1000,
                height: 1000,
                compressImageQuality:0.4,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    if (this.state.isLicence) {
                        let obj =  `data:${image.mime};base64,${image.data}`
                        let images = this.state.driving_licence;
                        images = [...images, obj]
                        this.setState({ driving_licence: images })
                    } else {
                        let obj = `data:${image.mime};base64,${image.data}`
                        let images = this.state.certification_id;
                        images = [...images, obj]
                        this.setState({ certification_id: images })
                    }
                });
        } else if (buttonIndex === 0) {
            ImagePicker.openCamera({
                width: 1000,
                height: 1000,
                compressImageQuality:0.4,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    if (this.state.isLicence) {
                        let obj = `data:${image.mime};base64,${image.data}`
                        let images = this.state.driving_licence;
                        images = [...images, obj]
                        this.setState({ driving_licence: images })
                    } else {
                        let obj = `data:${image.mime};base64,${image.data}`
                        let images = this.state.certification_id;
                        images = [...images, obj]
                        this.setState({ certification_id: images })
                    }
                });
        }
    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Upload your Documents'}
                    />
                </View>
                <Mutation mutation={UPLOAD_DOCUMENTS} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                {(uploadDocuments, { loading }) => (
                <SafeAreaView style={{ flex: 1 }}>

                    <View style={{ flex: 1 }}>
                        {/* Licence */}
                        <View style={{ flex: 1 }}>
                            <View style={{ marginHorizontal: 10, marginTop: 15 }}>
                                <LightText style={{ marginVertical: 10, marginLeft: 15, color: colors.dimGray, fontSize: 16 }}>Driving Licence Number</LightText>
                                <View style={{ height: 1, backgroundColor: colors.lightGray }} />
                                <TouchableOpacity style={{ height: 40, borderRadius: 5, backgroundColor: 'rgb(200,240,227)', flexDirection: 'row', alignItems: 'center', marginVertical: 20, borderStyle: 'dashed', borderWidth: .5, borderColor: 'gray' }}
                                    onPress={() => {
                                        this.setState({ isLicence: true })
                                        this.showActionSheet()
                                    }}
                                >
                                    <Image
                                        style={{ height: 25, width: 25, marginHorizontal: 20 }}
                                        source={require("../assets/photo-camera.png")}
                                        resizeMode={'contain'}
                                    />
                                    <LightText style={{ marginLeft: 10, color: colors.dimGray, fontSize: 14 }}>License</LightText>
                                </TouchableOpacity>
                            </View>
                            <FlatList
                                data={this.state.driving_licence}
                                horizontal
                                style={{ padding: 10 }}
                                ItemSeparatorComponent={() => {
                                    return <View style={{ flex: 1, width: 5 }} />
                                }}
                                renderItem={({ item, index }) => (
                                    <View style={{ flex: 1, paddingHorizontal: 5, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                                        <View style={[General.Card, { flex: 1, marginVertical: 5, alignItems: 'center', justifyContent: 'center', }]} onLayout={(event) => {
                                            var { height } = event.nativeEvent.layout;
                                            this.setState({ flatListheight: height })
                                        }}>
                                            <Image
                                                source={{ uri: `${item}` }}
                                                style={{ borderRadius: 5, height: this.state.flatListheight, width: this.state.flatListheight }}
                                                resizeMode={'contain'}
                                            />
                                        </View>
                                        <TouchableOpacity style={{ position: 'absolute', height: 20, width: 20, alignItems: 'flex-start', justifyContent: 'flex-start' }}
                                            onPress={() => {
                                                var array = [...this.state.driving_licence];
                                                array.splice(index, 1);
                                                this.setState({ driving_licence: array });
                                            }}
                                        >
                                            <Image
                                                source={require('../assets/remove.png')}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                )}
                            />

                        </View>
                        {/* Certificate */}
                        <View style={{ flex: 1 }}>
                            <View style={{ marginHorizontal: 10, marginTop: 15 }}>
                                <LightText style={{ marginVertical: 10, marginLeft: 15, color: colors.dimGray, fontSize: 16 }}>Certification ID</LightText>
                                <View style={{ height: 1, backgroundColor: colors.lightGray }} />

                                <TouchableOpacity style={{ height: 40, borderRadius: 5, backgroundColor: 'rgb(200,240,227)', flexDirection: 'row', alignItems: 'center', marginVertical: 20, borderStyle: 'dashed', borderWidth: .5, borderColor: 'gray' }}
                                    onPress={() => {
                                        this.setState({ isLicence: false })
                                        this.showActionSheet()
                                    }}
                                >
                                    <Image
                                        style={{ height: 25, width: 25, marginHorizontal: 20 }}
                                        source={require("../assets/photo-camera.png")}
                                        resizeMode={'contain'}
                                    />
                                    <LightText style={{ marginLeft: 10, color: colors.dimGray, fontSize: 14 }}>Upload Certificate</LightText>
                                </TouchableOpacity>
                            </View>
                            <FlatList
                                data={this.state.certification_id}
                                horizontal
                                style={{ padding: 10 }}
                                ItemSeparatorComponent={() => {
                                    return <View style={{ flex: 1, width: 5 }} />
                                }}
                                renderItem={({ item, index }) => (
                                    <View style={{ flex: 1, paddingHorizontal: 5, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                                        <View style={[General.Card, { flex: 1, marginVertical: 5, alignItems: 'center', justifyContent: 'center', }]}>
                                            <Image
                                                source={{ uri: `${item}` }}
                                                style={{ borderRadius: 5, height: this.state.flatListheight, width: this.state.flatListheight }}
                                                resizeMode={'contain'}
                                            />
                                        </View>
                                        <TouchableOpacity style={{ position: 'absolute', height: 20, width: 20, alignItems: 'flex-start', justifyContent: 'flex-start' }}
                                            onPress={() => {
                                                var array = [...this.state.certification_id];
                                                array.splice(index, 1);
                                                this.setState({ certification_id: array });
                                            }}
                                        >
                                            <Image
                                                source={require('../assets/remove.png')}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                )}
                            />
                        </View>
                        {/* Action sheet */}
                        <ActionSheet
                            ref={o => this.ActionSheet = o}
                            title={'Which one do you like ?'}
                            options={['Take photo', 'Choose from Library', 'Cancel']}
                            cancelButtonIndex={2}
                            onPress={(index) => { this.openGalleryForProfilePic(index) }}
                        />

                    </View>
                    <View style={{ marginVertical: 20, marginHorizontal: 10 }}>
                        <Button
                            style={{ borderRadius: 10, height: 45 }}
                            value={'Next'}
                            color={colors.appColor}
                            Light={true}
                            textStyle={{ fontSize: 20 }}
                            onPress={() => {
                                console.log(JSON.stringify({ variables: { driving_licence: this.state.driving_licence, certification_id: this.state.certification_id }}))
                                 uploadDocuments({ variables: { driving_licence: this.state.driving_licence, certification_id: this.state.certification_id }})
                            }}
                        />
                    </View>
                    <Loader loading={loading} />
                </SafeAreaView>
                )}
                </Mutation>
            </View>
        )
    }
}

const UPLOAD_DOCUMENTS = gql`
mutation uploadYourLicence($driving_licence: [String], $certification_id: [String]){
    uploadYourLicence(driving_licence: $driving_licence, certification_id: $certification_id) {
        success
        message
            }
          }`;