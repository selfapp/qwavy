import React, { Component } from "react";
import {
    TextInput,
    View,
    Image,
    Text,
    SafeAreaView,
    TouchableOpacity
} from "react-native";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import ErrorHandler from '../Components/ErrorHandler';
import {
    LightText,
    BoldText,
    TextInputField
} from './../Components/styledTexts';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from '../styles/colors';



export default class SignUp extends Component {
    constructor() {
        super();
        this.state = {
            isAccept: false,
            name: '',
            email: '',
            password: ''
        };
    }

    onSuccess = (data) => {
        console.log(JSON.stringify(data))
        if (data.register.success) {
            AsyncStorage.setItem('token', data.register.access_token).then(() => {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'completeProfile' })],
                });
                this.props.navigation.dispatch(resetAction);
            })
        } else {
            alert(data.register.message)
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Mutation mutation={SIGNUP} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                    {(signUp, { loading }) => (
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1, marginHorizontal: 30, alignItems: 'flex-start', justifyContent: 'flex-end' }}>
                                <Image
                                    source={require('../assets/logo.png')}
                                />
                                <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }}>
                                    <LightText style={{ fontWeight: '400', fontSize: 22, color: colors.dimGray }}>Create your</LightText>
                                    <BoldText style={{ fontSize: 24 }}> Account</BoldText>
                                </View>
                            </View>
                            <View style={{ flex: 2, justifyContent: 'space-evenly', paddingHorizontal: 30 }}>
                                <View >
                                    <LightText style={{}}>Full name</LightText>
                                    <View style={{ marginTop: 5 }}>
                                        <TextInputField
                                            placeholder={'Enter Full Name'}
                                            onChangeText={(name) => this.setState({ name: name })}
                                        />
                                        <View style={{ height: 1, marginTop: 3, backgroundColor: 'gray' }} />
                                    </View>
                                </View>
                                <View >
                                    <LightText style={{}}>Email ID</LightText>
                                    <View style={{ marginTop: 5 }}>
                                        <TextInputField
                                            placeholder={'Enter Email Address'}
                                            keyboardType={'email-address'}
                                            onChangeText={(email) => this.setState({ email: email })}
                                        />
                                        <View style={{ height: 1, marginTop: 3, backgroundColor: 'gray' }} />
                                    </View>
                                </View>
                                <View>
                                    <LightText style={{}}>Password</LightText>
                                    <View style={{ marginTop: 5 }}>
                                        <TextInputField
                                            placeholder={'Enter Password'}
                                            secureTextEntry={true}
                                            onChangeText={(password) => this.setState({ password: password })}
                                        />

                                        <View style={{ height: 1, marginTop: 3, backgroundColor: 'gray' }} />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center' }}
                                        onPress={() => this.setState({ isAccept: !this.state.isAccept })}
                                    >
                                        <Image
                                            source={(this.state.isAccept) ? require('../assets/check_box1.png') : require('../assets/check_box.png')}
                                            style={{ height: 20, width: 20 }}
                                        />
                                    </TouchableOpacity>
                                    <LightText>Term and Conditions*</LightText>
                                </View>
                            </View>
                            <View style={{ flex: 1, marginHorizontal: 30 }}>
                                <View style={{ flex: 1 }}>
                                    <Button
                                        style={{ borderRadius: 10, height: 50 }}
                                        value={'Sign up'}
                                        color={colors.appColor}
                                        Light={true}
                                        textStyle={{ fontSize: 20 }}
                                        onPress={() => {
                                            if (this.state.name.length > 1 && this.state.email.length > 1 && this.state.password.length > 1) {
                                                if(this.state.isAccept){
                                                signUp({ variables: { name: this.state.name, email: `${this.state.email}`, password: `${this.state.password}` } })
                                                }else{
                                                    alert('Please accept term and conditions.')
                                                }
                                            } else {
                                                alert('All fields are required.')
                                            }
                                        }}
                                    />
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ fontSize: 16, marginTop: 30 }}>Already have account?
                                <Text style={{ fontWeight: '800', color: colors.appColor }} onPress={() => this.props.navigation.goBack()}> Log in</Text>
                                    </Text>
                                </View>
                                <View style={{ height: 30 }} />
                            </View>
                            <Loader loading={loading} />
                        </View>
                    )}
                </Mutation>
            </SafeAreaView>
        )
    }
}

const SIGNUP = gql`
mutation register($name: String!, $email: String!, $password: String!){
    register(name: $name, email: $email, password: $password) {
                id
                name
                access_token
                success
                message
            }
          }`;