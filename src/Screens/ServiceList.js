import React, { Component } from "react";
import {
    View,
    Image,
    FlatList,
    SafeAreaView,
    TouchableOpacity
} from "react-native";
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import ErrorHandler from '../Components/ErrorHandler';
import {
    LightText,
    BoldText,
} from './../Components/styledTexts';
import Header from '../Components/Header';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from '../styles/colors';

export default class ServiceList extends Component {
    constructor() {
        super();
        this.state = {
            categoryId:0,
            serviceName:'',
            subCategoryList:[]
        };
    }

    componentDidMount(){
        this.setState({ categoryId: this.props.navigation.state.params.serviceId, serviceName: this.props.navigation.state.params.serviceName})
   }

   onSuccess = (data) => {
    console.log('Success', data)
    if (data.userServices.success) {
        this.props.navigation.navigate('PersonalInformation')
    } else {
        alert(data.userServices.message)
    }
}

   getDataResponse(data){
    var subcategories = data.getSubcategories
    //  console.log('ghghjg',subcategories)
        subcategories.map((item, index) => {
        item.isSelect = false;  
        subcategories[index] = item 
                   if(index === (subcategories.length-1)){
                    this.setState({ subCategoryList: subcategories});
                   }                              
      });
   }

   selectItem = (item) => {
    //    console.log(item)
    item.isSelect = !item.isSelect;
  
    const index = this.state.subCategoryList.findIndex(
      item => {
          item.id === item.id
        }
    );
  
    this.state.subCategoryList[index] = item;
  
    this.setState({
      subCategoryList: this.state.subCategoryList,
    });
  };


    renderItem = ({ item}) =>{
        return(
            <View style={{paddingVertical: 2,}}>
            <TouchableOpacity style={{ flex: 1, borderRadius: 1, backgroundColor: '#FFF', paddingVertical: 10,  shadowOffset:{width:0, height:2}, shadowOpacity: 0.18, shadowRadius:1, shadowColor: '#451B2D', elevation: 3 }}
                onPress={()=>this.selectItem(item)}
            >
                <View style={{ flex: 1, flexDirection:'row',}}>
                    <Image
                        source={require('../assets/Oval_.png')}
                        style={{tintColor: item.isSelect ? colors.appColor : colors.lightGray}}
                    />
                    <View style={{ flex: 1, marginLeft: 10, justifyContent:'center'}}>
                        <LightText style={{ fontSize: 18}}>{item.title}</LightText>
                    </View>
                    <View>
                    <Image
                        source={{uri: item.image}}
                        style={{ height: 60, width: 60, marginRight: 5, }}
                    />
                    </View>
                </View>
            </TouchableOpacity>
            </View>
        )}


    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{flex: .12}}>
                    <Header
                    leftNavigation={ this.props.navigation}
                    value={`${this.state.serviceName} Services`}
                    />
                </View>
            <SafeAreaView style={{ flex: 1 }}>
                
                <View style={{ flex: 1}}>
                <Query query={GET_SUB_CATEGORY} variables={{category_id: this.state.categoryId}} onCompleted={(data)=> this.getDataResponse(data)}>
                            {({loading, error}) => {
                                if(loading){
                                    return <Loader loading={loading} />
                                }
                                if(error) {
                                    return <BoldText>{error}</BoldText>
                                }
                                return <BoldText></BoldText>
                            }}
                        </Query>

                        <FlatList
                            data = {this.state.subCategoryList}
                            keyExtractor = {(item, index) => index.toString()}
                            style={{ flex: 1 }}
                            renderItem = {this.renderItem}
                        />
                </View>
                <Mutation mutation={USER_SERVICES} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                    {(saveCategories, { loading }) => (
                        <View>
                    <View style={{ marginVertical: 20, marginHorizontal: 10}}>
                            <Button
                                    style={{ borderRadius: 10, height: 45 }}
                                    value={'Continue'}
                                    color={colors.appColor}
                                    Light={true}
                                    textStyle={{fontSize: 20}}
                                    onPress ={() =>{
                                        let selectedIds = []
                                       this.state.subCategoryList.map((item, index) => {
                                           if(item.isSelect){
                                               selectedIds.push(item.id)
                                           }
                                           if(index === this.state.subCategoryList.length-1){
                                               console.log({ variables: { category_id: this.state.categoryId, subcategory_id: selectedIds } })
                                               saveCategories({ variables: { category_id: this.state.categoryId, subcategory_id: selectedIds } })
                                           }
                                       })
                                        // this.props.navigation.navigate('PersonalInformation')
                                    }}
                                />
                            </View>  
                <Loader loading={loading} />
                </View>
                )}
                </Mutation>
            </SafeAreaView>
            </View>
        )
    }
}

const GET_SUB_CATEGORY = gql`
        query avail($category_id: ID!){
            getSubcategories(category_id: $category_id) {
                id
                title
                image
            }
          }`;

const USER_SERVICES = gql`
mutation userServices($category_id: ID!, $subcategory_id: [ID!]!){
    userServices(category_id: $category_id, subcategory_id: $subcategory_id) {
        success
        message
            }
          }`;