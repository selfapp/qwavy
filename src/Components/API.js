import AsyncStorage from '@react-native-community/async-storage';
 let baseURL = 'http://3.130.153.144/api/';


export default class API {
    static baseURL = baseURL;

    static request(url, method = 'GET', body = null) {
        return AsyncStorage.getItem('accessToken').then((data) => {
            let access_token = data;
            return fetch(baseURL + url, {
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + (access_token ? access_token : null)
                },
                body: body === null ? null : JSON.stringify(body)
            });
        });
    }

    static login(country_code, phone_number) {
        return this.request('generate-otp', 'POST', {
                phone: phone_number,
                country_code: country_code,
                signup_by:'mobile',
                
        });
    }
    static reSendOTP(country_code, phone_number){
        return this.request('generate-otp', 'POST',{
                phone: phone_number,
                country_code: country_code,
                signup_by:'mobile'
        });
    }

    static verifyOTP(code, country_code, phone_number, userType) {
        console.log(code, phone_number)
        return this.request('confirm-verification-code', 'POST', {
                verification_code: code,
                phone: phone_number,
                country_code:country_code,
                is_business_account: userType
        });
    }
    
    }

