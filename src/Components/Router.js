import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import colors from '../styles/colors';
import Splash from '../Screens/Splash';
import Login from '../Screens/Login';
import ForgotPassword from '../Screens/ForgotPassword';
import SignUp from '../Screens/SignUp';
import Services from '../Screens/Services';
import ServiceList from '../Screens/ServiceList';
import PersonalInformation from '../Screens/PersonalInformation';
import UploadDocuments from '../Screens/UploadDocuments';
import BusinessPortfolio from '../Screens/BusinessPortfolio';
import Jobs from '../Screens/Jobs';
import Profile from '../Screens/Profile';
import Setting from '../Screens/Setting';
import About from '../Screens/About';
import FAQ from '../Screens/FAQ';
import ResetPassword from '../Screens/ResetPassword';
import EditProfile from '../Screens/EditProfile';
import ShowPortfolio from '../Screens/ShowPortfolio';


export const signUpNaviationOptions = createStackNavigator({
    Login: { screen: Login },
    ForgotPassword: { screen: ForgotPassword},
    SignUp: { screen: SignUp},
    Services: { screen: Services},
    ServiceList: { screen: ServiceList},
    PersonalInformation: { screen: PersonalInformation},
    UploadDocuments: { screen: UploadDocuments },
    BusinessPortfolio: { screen: BusinessPortfolio}
},{
    headerMode: 'none'
})
export const completeProfile = createStackNavigator({
    Services: { screen: Services},
    ServiceList: { screen: ServiceList},
    PersonalInformation: { screen: PersonalInformation},
    UploadDocuments: { screen: UploadDocuments },
    BusinessPortfolio: { screen: BusinessPortfolio}
},{
    headerMode: 'none'
})

export const profileNavigation = createStackNavigator({
    Profile: { screen: Profile},
    Setting: { screen: Setting},
    About : { screen: About},
    FAQ: { screen: FAQ },
    ResetPassword: { screen: ResetPassword },
    EditProfile: { screen: EditProfile },
    BusinessPortfolio: { screen: BusinessPortfolio},
    ShowPortfolio: { screen: ShowPortfolio}
},{
    headerMode: 'none'
})

export const bottomTabNavigator = createBottomTabNavigator(
    {
        Jobs:{
            screen: Jobs,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/jobs.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        Messages:{
            screen: Jobs,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/message.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        Profile:{
          screen: profileNavigation,
          navigationOptions:{
              tabBarIcon: ({tintColor}) =>{
                  return(
                      <Image
                      source = {require('../assets/user.png')}
                      style = {{tintColor:tintColor}}
                      />
                  )
              }
          }
      },
    },
    {
        initialRouteName: 'Jobs',        
        tabBarOptions: {
            activeTintColor: colors.appColor,
            inactiveTintColor: colors.gray,
            showLabel: true,
            showIcon: true,
            tabBarPosition: 'bottom',
            labelStyle: {
                fontSize: 12,
            },
            iconStyle:{
                width: 30,
                height: 30
            },
            style: {
                backgroundColor: 'rgb(245,245,245)',
                borderBottomWidth: 1,
                borderBottomColor: '#ededed',
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
            },
            lazy: true,
            indicatorStyle: '#fff',
        }
    }
  );


export const appNavigationOptions = createStackNavigator({
    Splash: { screen: Splash},
    signUpNaviationOptions: { screen: signUpNaviationOptions},
    completeProfile: { screen: completeProfile},
    bottomTabNavigator: { screen: bottomTabNavigator}
},{
    headerMode: 'none'
})

export const AppContainer = createAppContainer(appNavigationOptions);