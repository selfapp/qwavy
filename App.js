/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { View } from 'react-native';
import { AppContainer } from './src/Components/Router';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient, InMemoryCache} from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import AsyncStorage from '@react-native-community/async-storage';


const httpLink = createHttpLink({
  uri: 'http://3.132.6.110/graphql',
});
const authLink = setContext((_, { headers }) => {
 return AsyncStorage.getItem('token').then((data) => {
  let token = data;
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});
});
const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
}
const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(httpLink),
  defaultOptions: defaultOptions,
  // link: new HttpLink({uri: 'http://13.52.145.212:8080/graphql'})
})

export default class App extends Component {

  constructor(props){
    super(props)
    // console.disableYellowBox = true
  }

  render() {
    return (
      <View style={{ flex:1}}>
            <ApolloProvider client={client}>
        <AppContainer/>
        </ApolloProvider>
      </View>
    );
  }
}

